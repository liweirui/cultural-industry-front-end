import request from '@/utils/request'

// 查询专家筛选条件列表
export function listCondition(query) {
  return request({
    url: '/system/condition/list',
    method: 'get',
    params: query
  })
}

// 查询所有专家筛选条件列表
export function listAllCondition() {
  return request({
    url: '/system/condition/selectAll',
    method: 'get',
  })
}

// 查询专家筛选条件详细
export function getCondition(conditionId) {
  return request({
    url: '/system/condition/' + conditionId,
    method: 'get'
  })
}

// 新增专家筛选条件
export function addCondition(data) {
  return request({
    url: '/system/condition',
    method: 'post',
    data: data
  })
}

// 修改专家筛选条件
export function updateCondition(data) {
  return request({
    url: '/system/condition',
    method: 'put',
    data: data
  })
}

// 删除专家筛选条件
export function delCondition(conditionId) {
  return request({
    url: '/system/condition/' + conditionId,
    method: 'delete'
  })
}

// 导出专家筛选条件
export function exportCondition(query) {
  return request({
    url: '/system/condition/export',
    method: 'get',
    params: query
  })
}
