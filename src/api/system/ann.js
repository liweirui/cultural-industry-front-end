import request from '@/utils/request'

// 查询项目申报公告列表
export function listAnn(query) {
  return request({
    url: '/system/ann/list',
    method: 'get',
    params: query
  })
}

// 查询项目申报公告详细
export function getAnn(announceTitle) {
  return request({
    url: '/system/ann/' + announceTitle,
    method: 'get'
  })
}

// 新增项目申报公告
export function addAnn(data) {
  return request({
    url: '/system/ann',
    method: 'post',
    data: data
  })
}

// 修改项目申报公告
export function updateAnn(data) {
  return request({
    url: '/system/ann',
    method: 'put',
    data: data
  })
}

// 删除项目申报公告
export function delAnn(announceTitle) {
  return request({
    url: '/system/ann/' + announceTitle,
    method: 'delete'
  })
}

// 导出项目申报公告
export function exportAnn(query) {
  return request({
    url: '/system/ann/export',
    method: 'get',
    params: query
  })
}