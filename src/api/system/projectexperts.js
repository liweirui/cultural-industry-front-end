import request from '@/utils/request'

// 查询项目专家组关联列表
export function listProjectexperts(query) {
  return request({
    url: '/system/projectexperts/list',
    method: 'get',
    params: query
  })
}

// 查询项目专家组关联详细
export function getProjectexperts(id) {
  return request({
    url: '/system/projectexperts/' + id,
    method: 'get'
  })
}

// 新增项目专家组关联
export function addProjectexperts(data) {
  return request({
    url: '/system/projectexperts',
    method: 'post',
    data: data
  })
}

// 修改项目专家组关联
export function updateProjectexperts(data) {
  return request({
    url: '/system/projectexperts',
    method: 'put',
    data: data
  })
}

// 删除项目专家组关联
export function delProjectexperts(id) {
  return request({
    url: '/system/projectexperts/' + id,
    method: 'delete'
  })
}

// 导出项目专家组关联
export function exportProjectexperts(query) {
  return request({
    url: '/system/projectexperts/export',
    method: 'get',
    params: query
  })
}


