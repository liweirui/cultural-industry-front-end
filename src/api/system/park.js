import request from '@/utils/request'

// 查询产业园区信息列表
export function listPark(query) {
  return request({
    url: '/system/park/list',
    method: 'get',
    params: query
  })
}

// 查询产业园区信息详细
export function getPark(parkId) {
  return request({
    url: '/system/park/' + parkId,
    method: 'get'
  })
}

// 新增产业园区信息
export function addPark(data) {
  return request({
    url: '/system/park',
    method: 'post',
    data: data
  })
}

// 修改产业园区信息
export function updatePark(data) {
  return request({
    url: '/system/park',
    method: 'put',
    data: data
  })
}

// 删除产业园区信息
export function delPark(parkId) {
  return request({
    url: '/system/park/' + parkId,
    method: 'delete'
  })
}

// 导出产业园区信息
export function exportPark(query) {
  return request({
    url: '/system/park/export',
    method: 'get',
    params: query
  })
}