import request from '@/utils/request'

// 查询项目申报材料列表
export function listMari(query) {
  return request({
    url: '/system/mari/list',
    method: 'get',
    params: query
  })
}

// 查询项目申报材料详细
export function getMari(id) {
  return request({
    url: '/system/mari/' + id,
    method: 'get'
  })
}

// 新增项目申报材料
export function addMari(data) {
  return request({
    url: '/system/mari',
    method: 'post',
    data: data
  })
}

// 修改项目申报材料
export function updateMari(data) {
  return request({
    url: '/system/mari',
    method: 'put',
    data: data
  })
}

// 删除项目申报材料
export function delMari(id) {
  return request({
    url: '/system/mari/' + id,
    method: 'delete'
  })
}

// 导出项目申报材料
export function exportMari(query) {
  return request({
    url: '/system/mari/export',
    method: 'get',
    params: query
  })
}