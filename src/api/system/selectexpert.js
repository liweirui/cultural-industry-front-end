import request from '@/utils/request'

// 查找符合筛选条件的专家
export function findExperts(query) {
  return request({
    url: '/system/select/find',
    method: 'get',
    params: query
  })
}

// 点击确定保存筛查的专家信息和关联表
export function addProjectexperts(data) {
  return request({
    url: '/system/select/sure',
    method: 'post',
    data: data
  })
}

// 设置 通过或不通过
export function setOnePass(query) {
  return request({
    url: '/system/select/setonepass',
    method: 'get',
    params: query
  })
}

// 列出选择的项目的筛查出来的具体专家的信息
export function list(query) {
  return request({
    url: '/system/select/list',
    method: 'get',
    params: query
  })
}

export function findConditionId(query) {
  return request({
    url: '/system/select/findConditionId',
    method: 'get',
    params: query
  })
}

export function findCondition(query) {
  return request({
    url: '/system/select/findCondition',
    method: 'get',
    params: query
  })
}

// 删除关联的数据
export function deleteTreeForm(query) {
  return request({
    url: '/system/select/deleteTreeForm',
    method: 'get',
    params: query
  })
}



