import request from '@/utils/request'

// 查询项目申报管理列表
export function listProject_issue(query) {
  return request({
    url: '/system/project_issue/list',
    method: 'get',
    params: query
  })
}

// 查询项目申报管理详细
export function getProject_issue(projectIssueId) {
  return request({
    url: '/system/project_issue/' + projectIssueId,
    method: 'get'
  })
}

// 新增项目申报管理
export function addProject_issue(data) {
  return request({
    url: '/system/project_issue',
    method: 'post',
    data: data
  })
}

// 修改项目申报管理
export function updateProject_issue(data) {
  return request({
    url: '/system/project_issue',
    method: 'put',
    data: data
  })
}

// 删除项目申报管理
export function delProject_issue(projectIssueId) {
  return request({
    url: '/system/project_issue/' + projectIssueId,
    method: 'delete'
  })
}

// 导出项目申报管理
export function exportProject_issue(query) {
  return request({
    url: '/system/project_issue/export',
    method: 'get',
    params: query
  })
}