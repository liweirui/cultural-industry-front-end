import request from '@/utils/request'

// 查询产业基金信息列表
export function listFund(query) {
  return request({
    url: '/system/fund/list',
    method: 'get',
    params: query
  })
}

// 查询产业基金信息详细
export function getFund(fundId) {
  return request({
    url: '/system/fund/' + fundId,
    method: 'get'
  })
}

// 新增产业基金信息
export function addFund(data) {
  return request({
    url: '/system/fund',
    method: 'post',
    data: data
  })
}

// 修改产业基金信息
export function updateFund(data) {
  return request({
    url: '/system/fund',
    method: 'put',
    data: data
  })
}

// 删除产业基金信息
export function delFund(fundId) {
  return request({
    url: '/system/fund/' + fundId,
    method: 'delete'
  })
}

// 导出产业基金信息
export function exportFund(query) {
  return request({
    url: '/system/fund/export',
    method: 'get',
    params: query
  })
}