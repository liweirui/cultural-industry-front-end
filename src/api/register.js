import request from '@/utils/request'

export function registerEnterprise(data){
  return request({
    url:'/register_enterprise',
    headers:{
      isToken:false
    },
    method:'post',
    data:data
  })
}


export function registerPark(data){
  return request({
    url:'/register_park',
    headers:{
      isToken:false
    },
    method:'post',
    data:data
  })
}

export function registerFund(data){
  return request({
    url:'/register_fund',
    headers:{
      isToken:false
    },
    method:'post',
    data:data
  })
}

export function registerOrganization(data){
  return request({
    url:'/register_organization',
    headers:{
      isToken:false
    },
    method:'post',
    data:data
  })
}

export function registerExpert(data){
  return request({
    url:'/register_expert',
    headers:{
      isToken:false
    },
    method:'post',
    data:data
  })
}
