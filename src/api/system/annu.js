import request from '@/utils/request'

// 查询项目申报公告列表
export function listAnnu(query) {
  return request({
    url: '/system/annu/list',
    method: 'get',
    params: query
  })
}

// 查询所有还未安排专家组的项目列表
export function listAllAnnu() {
  return request({
    url: '/system/select/selectAllProject',
    method: 'get',
  })
}

// 查询项目申报公告详细
export function getAnnu(pjannId) {
  return request({
    url: '/system/annu/' + pjannId,
    method: 'get'
  })
}

// 新增项目申报公告
export function addAnnu(data) {
  return request({
    url: '/system/annu',
    method: 'post',
    data: data
  })
}

// 修改项目申报公告
export function updateAnnu(data) {
  return request({
    url: '/system/annu',
    method: 'put',
    data: data
  })
}

// 删除项目申报公告
export function delAnnu(pjannId) {
  return request({
    url: '/system/annu/' + pjannId,
    method: 'delete'
  })
}

// 导出项目申报公告
export function exportAnnu(query) {
  return request({
    url: '/system/annu/export',
    method: 'get',
    params: query
  })
}
