import request from '@/utils/request'

// 查询通知公告管理列表
export function listNotification(query) {
  return request({
    url: '/system/notification/list',
    method: 'get',
    params: query
  })
}

export function listNotificationByType(query) {
  return request({
    url: '/system/notification/typeList',
    method: 'get',
    params: query
  })
}

export function listNotificationByType2(query) {
  return request({
    url: '/system/notification/typeList2',
    method: 'get',
    params: query
  })
}

export function listNotificationAll() {
  return request({
    url: '/system/notification/typeListAll',
    method: 'get',
  })
}

// 查询通知公告管理详细
export function getNotification(notificationId) {
  return request({
    url: '/system/notification/' + notificationId,
    method: 'get'
  })
}

// 新增通知公告管理
export function addNotification(data) {
  return request({
    url: '/system/notification',
    method: 'post',
    data: data
  })
}

// 修改通知公告管理
export function updateNotification(data) {
  return request({
    url: '/system/notification',
    method: 'put',
    data: data
  })
}

// 删除通知公告管理
export function delNotification(notificationId) {
  return request({
    url: '/system/notification/' + notificationId,
    method: 'delete'
  })
}

// 导出通知公告管理
export function exportNotification(query) {
  return request({
    url: '/system/notification/export',
    method: 'get',
    params: query
  })
}
