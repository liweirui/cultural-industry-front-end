import request from '@/utils/request'

// 查询行业组织管理信息列表
export function listOrganization(query) {
  return request({
    url: '/system/organization/list',
    method: 'get',
    params: query
  })
}

// 查询行业组织管理信息详细
export function getOrganization(orgId) {
  return request({
    url: '/system/organization/' + orgId,
    method: 'get'
  })
}

// 新增行业组织管理信息
export function addOrganization(data) {
  return request({
    url: '/system/organization',
    method: 'post',
    data: data
  })
}

// 修改行业组织管理信息
export function updateOrganization(data) {
  return request({
    url: '/system/organization',
    method: 'put',
    data: data
  })
}

// 删除行业组织管理信息
export function delOrganization(orgId) {
  return request({
    url: '/system/organization/' + orgId,
    method: 'delete'
  })
}

// 导出行业组织管理信息
export function exportOrganization(query) {
  return request({
    url: '/system/organization/export',
    method: 'get',
    params: query
  })
}