import request from '@/utils/request'

// 查询专家信息列表
export function listExpert(query) {
  return request({
    url: '/system/expert/list',
    method: 'get',
    params: query
  })
}

// 查询专家信息详细
export function getExpert(expertId) {
  return request({
    url: '/system/expert/' + expertId,
    method: 'get'
  })
}

// 新增专家信息
export function addExpert(data) {
  return request({
    url: '/system/expert',
    method: 'post',
    data: data
  })
}

// 修改专家信息
export function updateExpert(data) {
  return request({
    url: '/system/expert',
    method: 'put',
    data: data
  })
}

// 删除专家信息
export function delExpert(expertId) {
  return request({
    url: '/system/expert/' + expertId,
    method: 'delete'
  })
}

// 导出专家信息
export function exportExpert(query) {
  return request({
    url: '/system/expert/export',
    method: 'get',
    params: query
  })
}