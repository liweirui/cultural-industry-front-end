import request from '@/utils/request'

// 查询用户个人信息
export function getMainMessage() {
  return request({
    url: '/system/userInformation/getMainMessage',
    method: 'get'
  })
}
